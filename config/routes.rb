Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
  namespace :admin do
    root 'static_pages#home'

    resources :galleries
    resources :images
    resources :sheets
  end
  namespace :en do
    root 'static_pages#home'
    resources :galleries
    resources :images
  end
end
