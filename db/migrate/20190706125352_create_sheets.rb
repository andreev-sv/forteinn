class CreateSheets < ActiveRecord::Migration[5.1]
  def change
    create_table :sheets do |t|
      t.text :text
      t.timestamps
    end
  end
end
