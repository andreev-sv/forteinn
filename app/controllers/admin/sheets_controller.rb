class Admin::SheetsController < Admin::ApplicationController
  def index
    @sheets = Sheet.all
  end
  def new
    @sheet = Sheet.new
  end
  def edit
  @sheet = Sheet.find(params[:id])
  end
  def create
    @sheet = Sheet.new(sheet_params)
    if @sheet.save
      redirect_to admin_sheets_path
    else
      render :new
    end
  end
  def update
    @sheet = Sheet.find(params[:id])
    if @sheet.update(sheet_params)
    redirect_to admin_sheets_path
    else
    render 'edit'
    end
  end
  def destroy
  @sheet = Sheet.find(params[:id])
  @sheet.destroy
  redirect_to admin_sheets_path
  end
  private
  def sheet_params
    params.require(:sheet).permit(:text)
  end
end
