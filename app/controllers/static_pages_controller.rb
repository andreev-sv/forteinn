class StaticPagesController < ApplicationController
  def home
    @galleries = Gallery.all
    @images = Image.all
    @sheets = Sheet.all
  end
end
